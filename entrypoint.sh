#!/bin/sh

# Apply database migrations
echo "Applying database migrations..."
python3 manage.py makemigrations api
python3 manage.py migrate
###############################################################################################################################################
# FOR DEVELOPMENT PURPOSES ONLY
python3 manage.py shell -c "from api.models import User; User.objects.create_superuser('root', 'root@localhost', 'root')"
###############################################################################################################################################

# Run commands
exec "$@"