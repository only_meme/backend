# Only Meme Backend

## Overview

This API facilitates a platform for users to share and engage with photographs. It provides functionality for uploading pictures along with titles and descriptions, viewing a curated list of shared pictures, accessing detailed views of individual pictures, editing picture information, and deleting pictures.

## Features

- **Upload Pictures**: Users can share their photographs with titles and descriptive text.
- **View Pictures**: A gallery view of all uploaded pictures is available.
- **Picture Details**: Detailed views for each picture, including title, description, and image view.
- **Update Information**: Edit capabilities for picture titles and descriptions.
- **Delete Pictures**: Option for users to remove their pictures from the platform.
## REST API



## How to contribute
