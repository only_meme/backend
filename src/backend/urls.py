from django.urls import path, include
from rest_framework import routers

from api import views

api = routers.DefaultRouter(trailing_slash=False)
api.register(r'users', views.UserViewSet)
api.register(r'pictures', views.PictureViewSet)

urlpatterns = [
    path('api/', include(api.urls)),
    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
]
