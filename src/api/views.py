from django.contrib.auth import get_user_model
from rest_framework import permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from api.serializers import UserSerializer, PictureSerializer
from api.permissions import IsSuperuserOrOwner
from api.models import Picture

User = get_user_model()

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [IsSuperuserOrOwner]

    def get_permissions(self):
        # Customize permissions for the 'register' action
        if self.action == 'register':
            return [permissions.AllowAny()]
        return super().get_permissions()

    @action(detail=False, methods=['post'], permission_classes=[permissions.AllowAny])
    def register(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            # Optionally send a confirmation email here
            return Response({
                "user": UserSerializer(user, context={'request': request}).data,
                "message": "User Created Successfully. Please confirm your email to activate your account.",
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['put'], detail=True, permission_classes=[permissions.IsAdminUser])
    def block(self, request, pk=None):
        """Allows superusers to block a user."""
        user = self.get_object()
        user.blocked = True
        user.save()
        return Response({"status": "user blocked"})

    @action(methods=['delete'], detail=True, permission_classes=[permissions.IsAdminUser])
    def delete_user(self, request, pk=None):
        """Allows superusers to delete a user."""
        user = self.get_object()
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class PictureViewSet(viewsets.ModelViewSet):
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer
    permission_classes = [permissions.IsAuthenticated]

