import uuid
from django.contrib.auth.models import AbstractUser
from django.db import models

class User(AbstractUser):
    username = models.CharField(
        'username',
        max_length=50,
        unique=True,
        help_text='Required. 50 characters or fewer. Letters, digits and @/./+/-/_ only.',
        error_messages={
            'unique': "A user with that username already exists.",
        },
    )
    email = models.EmailField(unique=True, blank=False,
                              error_messages={
                                  'unique': "A user with that email already exists.",
                              })
    gender = models.CharField(max_length=20)
    blocked = models.BooleanField(default=False)
    is_confirmed = models.BooleanField(default=False)
    about = models.TextField(blank=True)

    USERNAME_FIELD = 'email'  # Use email for logging in
    REQUIRED_FIELDS = ['username'] 

    groups = None
    user_permissions = None

    def __unicode__(self):
        return self.email

def uuid_upload_to(instance, filename):
    extension = filename.split('.')[-1]
    # Generate a filename based on uuid4
    new_filename = f"{uuid.uuid4()}.{extension}"
    return f"{new_filename}"

class Picture(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to=uuid_upload_to)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

