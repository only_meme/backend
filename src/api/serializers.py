from rest_framework import serializers

from api.models import User, Picture

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'gender', 'blocked', 'about', 'is_confirmed']
        extra_kwargs = {
            'password': {'write_only': True},
            'blocked': {'read_only': True},
            'is_confirmed': {'read_only': True}
        }

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
            gender=validated_data.get('gender', ''),
            about=validated_data.get('about', ''),
            blocked=False,
            is_confirmed=False  # Users are not confirmed at registration
        )
        return user

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.about = validated_data.get('about', instance.about)
        if 'password' in validated_data:
            instance.set_password(validated_data['password'])
        instance.save()
        return instance

class PictureSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Picture
        fields = ['id', 'title', 'image', 'created_at']