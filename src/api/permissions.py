from rest_framework import permissions

class IsSuperuserOrOwner(permissions.BasePermission):
    """
    Custom permission that allows superusers to perform any action, and regular users to update their own profile.
    """

    def has_permission(self, request, view):
        # Allow superusers to do anything
        if request.user.is_authenticated and request.user.is_superuser:
            return True

        # For other users, check if they are trying to modify themselves, except for destructive actions
        if view.action in ['list', 'update', 'partial_update', 'retrieve']:
            if request.user.is_authenticated:
                return True

        return False

    def has_object_permission(self, request, view, obj):
        # Superusers can modify anyone
        if request.user.is_superuser:
            return True

        # Users can only modify themselves
        return obj == request.user